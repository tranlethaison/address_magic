"""Japanese address analyzer
180207 - SonTLT - create
"""
from unicodedata import normalize
from difflib import SequenceMatcher

import google

# _position_free_chars = ['大', '字']

_noise_chars = [
    '(', ')',
    '（', '）',
    '丁', '目',
    '大', '字'
]


def is_match(address_0, address_1):
    if address_0 is None \
    or address_0 == '' \
    or address_1 is None \
    or address_1 == '':
        return address_0 == address_1

    address_0 = normalize('NFKC', address_0)
    address_1 = normalize('NFKC', address_1)

    # for char in _position_free_chars:
    #     if address_0.count(char) == address_1.count(char):
    #         address_0 = address_0.replace(char, '')
    #         address_1 = address_1.replace(char, '')

    for char in _noise_chars:
        address_0 = address_0.replace(char, '')
        address_1 = address_1.replace(char, '')

    return SequenceMatcher(isjunk=None, a=address_0, b=address_1).ratio() == 1.0


def is_match_by_google(address_0, address_1):
    try:
        if address_0 is None \
        or address_0 == '' \
        or address_1 is None \
        or address_1 == '':
            return address_0 == address_1

        attempts = 10
        while attempts > 0:
            attempts -= 1
            response0 = google.geocode(address_0).json()
            if response0['status'] != 'OVER_QUERY_LIMIT':
                break

        attempts = 10
        while attempts > 0:
            attempts -= 1
            response1 = google.geocode(address_1).json()
            if response1['status'] != 'OVER_QUERY_LIMIT':
                break

        error_responses = [
            'OVER_QUERY_LIMIT',
            'REQUEST_DENIED',
            'INVALID_REQUEST',
            'UNKNOWN_ERROR'
        ]

        if response0['status'] in error_responses \
        or response1['status'] in error_responses:
            raise Exception(
                '[' + address_0 + ': ' + response0['status'] + ']' + \
                '[' + address_1 + ': ' + response1['status'] + ']'
            )

        if response0['status'] == 'ZERO_RESULTS' \
        and response1['status'] == 'ZERO_RESULTS':
            return address_0 == address_1

        if response0['status'] == 'OK' \
        and response1['status'] == 'OK':
            place_ids_0 = set([
                result['place_id']
                for result in response0['results']
            ])
            place_ids_1 = set([
                result['place_id']
                for result in response1['results']
            ])
            return len(place_ids_0.intersection(place_ids_1)) > 0
    except Exception as ex:
        raise ex
