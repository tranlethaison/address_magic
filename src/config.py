'''
user config

History
> 171020 - SonTLT - Create
> 171026 - SonTLT - Fix Google Maps URL
> 171102 - SonTLT - Add system config
'''
from os import pardir, path
from sys import exc_info
from configparser import ConfigParser
import re
from selenium import webdriver

USER_CONFIG = {
    'browser': 'Chrome',
    'to_use_google_geocoding_api': '1',
    'original_addresses_column': 'E',
    'google_coordinates_column': 'F',
    'yahoo_addresses_column': 'G',
    'start_from_row': 3,
    'end_at_row': 1000,
    'delay': 5
}

USER_CONFIG_FILE = path.join(pardir, 'user_config.ini')


def create_config():
    """
    Create config file if it dose not exists.
    Create any option with default value if it doesn't exists
    """
    try:
        # User config
        if not path.exists(USER_CONFIG_FILE):
            open(USER_CONFIG_FILE, mode='a').close()

        user_config = ConfigParser(allow_no_value=True)
        user_config.read(USER_CONFIG_FILE)

        for key in USER_CONFIG:
            if(not user_config.has_option('DEFAULT', key)
               or (user_config.get('DEFAULT', key) == '')):
                user_config.set('DEFAULT', key, str(USER_CONFIG[key]))

        with open(USER_CONFIG_FILE, mode='w') as user_config_:
            user_config.write(user_config_)

    except Exception as e:
            exc_type, exc_obj, exc_tb = exc_info()
            fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            raise Exception(fname, exc_tb.tb_lineno, exc_info())


def get_config(option):
    """
    Get the section.
    """
    try:
        user_config = ConfigParser(allow_no_value=True)
        user_config.read(USER_CONFIG_FILE)

        if user_config.has_option('DEFAULT', option):
            value = user_config.get('DEFAULT', option)
        else:
            value = None

        return value
    except Exception as e:
            exc_type, exc_obj, exc_tb = exc_info()
            fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            raise Exception(fname, exc_tb.tb_lineno, exc_info())


def quote(str_, quote=None):
    if not quote:
        return str_
    return quote + str_ + quote
