"""
> Yahoo! web operator
> Created by SonTLT - 171221
> History:

"""
import re
import sys
from time import sleep

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

import jp_address_analyzer as analyzer
from config import get_config


class Coors:
    """Use Yahoo Maps to get coordinates from address
    """
    url = r'http://user.numazu-ct.ac.jp/~tsato/webmap/sphere/coordinates/yahoo_olp/'

    def __init__(self, web_driver):
        self.delay = float(get_config('delay'))
        self.driver = web_driver
        self.reset()

    def __del__(self):
        self.driver.quit()

    def get(self, address):
        """get coordinates"""
        self.reset()
        self.addrress = address

        self.get_candidates_choose_winner(address)
        if self.winner > -1:
            self.click_candidate(self.winner)
            if self.is_candidate_be_clicked():
                self.get_coors()

    def reset(self):
        """before working with new master address"""
        self.addrress = ''  # master address
        self.count = 0  # candidates count
        self.candidates = []  # list of candidate addresses
        self.winner = -1  # winner between candidates
        self.current = 0  # current checking candidate id
        self.latitude = ''
        self.longitude = ''

    def get_candidates_choose_winner(self, addr):
        """ get candidates and choose 1 winner in the process
        """
        # count candidates of main address
        self.count = self.count_candidates(self.driver, addr)
        if self.count == 0:
            return

        is_1st_page = True
        while True:
            try_time = 5
            while try_time > 0:
                if self.is_candidates_ready_to_update(): 
                    self.update_candidates()
                    break
                try_time -= 1
                sleep(2)

            # pick a winner candidate with JPAddress Analyzer
            while self.current < len(self.candidates):
                if analyzer.is_match(addr, self.candidates[self.current]):
                    self.winner = self.current
                    return
                self.current += 1

            if self.current == self.count:
                return
            elif is_1st_page:
                self.click_more_btn()  # もっと見る
                is_1st_page = False
            else:
                self.click_next_btn()  # 次の\d件

    def begin_search(self, driver, addr):
        """ Patse the address into search text, hit Return
        """
        driver.get(self.url)

        # <input type="text" class="yolp-address-search-txtbx"
        try:
            search_text = \
                WebDriverWait(driver, self.delay).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'yolp-address-search-txtbx'))
                )
        except TimeoutException as ex:
            ex.msg = '[Time out][begin_search] <input type="text" class="yolp-address-search-txtbx"'
            sys.exit(ex)

        search_text.send_keys(addr + Keys.RETURN)

    def count_candidates(self, driver, addr):
        """
        > Count search result
        > Paras:
            address
        """
        self.begin_search(driver, addr)

        # find result 
        # <dl class="rthrlst">
        try:
            result_dl = \
                WebDriverWait(driver, self.delay).until(
                    EC.presence_of_element_located((By.CLASS_NAME, 'rthrlst'))
                )

            result = result_dl.text.split('\n')  # result list
            """ex:
            ['住所との一致(32件中1～5 件目)',
             '住所',
             '埼玉県川越市豊田本1丁目',
             '埼玉県川越市豊田本1丁目1',
             '埼玉県川越市豊田本1丁目2',
             '埼玉県川越市豊田本1丁目3',
             '埼玉県川越市豊田本1丁目4']
            """
        except TimeoutException as ex:
            # ex.msg = '[Time out][count_candidates] <dl class="rthrlst">'
            # print(ex)
            return 0

        if result:
            if not re.search(r'^住所.+', result[0]):
                return 0

            count = re.search(
                r'\w+\W*(\d+)件.+',
                result[0]
            )
            if count:
                return int(count[1])
            else:
                return 0
        
    def is_candidates_ready_to_update(self):
        start, end = self.get_candidates_current_range()
        return self.current == start - 1

    def get_candidates_current_range(self):
        """get the current 住所との一致 's range"""
        try:
            list_title = WebDriverWait(self.driver, self.delay).until(
                EC.visibility_of_element_located((
                    By.CSS_SELECTOR, 
                    '#map_canvas > div.yolp-address-search > div.yolp-address-search-body > div > dl > dt'
                ))
            )
        except TimeoutException as ex:
            ex.msg = '[Time out][get_candidates_range]'
            raise ex
            
        """2 cases:
        住所との一致(32件中1～5 件目)
        住所との一致(1件中1 件目)
        """
        range_ = re.search(
            pattern=\
            r"""
            住所との一致
            \(\s*\d+\s*件中
            \s*(\d+)\s* #start
            ～?
            \s*(\d*)\s* #end
            件目\)
            """,
            string=list_title.text,
            flags=re.VERBOSE
        )
        
        if range_ is not None:
            start, end = range_[1], range_[2]
            if end == '': end = -1
            return map(int, [start, end])
        else:
            raise Exception('[re][func: get_candidates_current_range]')

    def update_candidates(self):
        """ Update candidate list
        """
        # take on screen list
        # <table class="addrndlst"
        try:
            screen_ls = \
                WebDriverWait(self.driver, self.delay).until(
                    EC.visibility_of_element_located((By.CLASS_NAME, 'addrndlst'))
                )
        except TimeoutException as ex:
            ex.msg = '[Time out][update_candidates] <table class="addrndlst"'
            print(ex)
            return

        if screen_ls:
            self.candidates.extend(screen_ls.text.split('\n')[1:])

    def click_more_btn(self):
        """ Click もっと見る JavaScript link
        """
        # <p class="more">
        try:
            more_btn = \
                WebDriverWait(self.driver, self.delay).until(
                    EC.visibility_of_element_located((
                        By.CSS_SELECTOR,
                        "#map_canvas > div.yolp-address-search > div.yolp-address-search-body > div > dl:nth-child(1) > dd > p > a"
                    ))
                )
        except TimeoutException as ex:
            ex.msg = '[func: click_more_btn][current: ' + str(self.current) + '][count: ' + str(self.count) + ']'
            raise ex
        more_btn.click()

    def click_next_btn(self):
        """ Click 次の\d件 JavaScript link
        """
        try:
            next_btn = \
                WebDriverWait(self.driver, self.delay).until(
                    EC.visibility_of_element_located((
                        By.CSS_SELECTOR,
                        "#map_canvas > div.yolp-address-search > div.yolp-address-search-body > div > dl > dd > p > div > a:nth-child(2)"
                    ))
                )
        except TimeoutException as ex:
            ex.msg = '[func: click_next_btn][current: ' + str(self.current) + '][count: ' + str(self.count) + ']'
            raise ex
        next_btn.click()

    def click_candidate(self, candidate_id):
        """"""
        # <table class="addrndlst"
        #     <tbody>
        #         <a href="javascript:void(0);" name="0">埼玉県川越市豊田本1丁目</a>

        try:
            candidate_link = \
                WebDriverWait(self.driver, self.delay).until(
                    EC.visibility_of_element_located((
                        By.CSS_SELECTOR,
                        "table[class='addrndlst'] a[name='" + str(candidate_id) + "']"
                    ))
                )
        except TimeoutException as ex:
            ex.msg = "[Time out][click_candidate] table[class='addrndlst'] a[name='" + str(candidate_id) + "']" + \
                '[address:' + self.addrress + ']'
            print(ex)
            return
        candidate_link.click()

    def is_candidate_be_clicked(self):
        try:
            WebDriverWait(self.driver, self.delay).until(
                EC.invisibility_of_element_located(
                    (By.CSS_SELECTOR, "#map_canvas > div.yolp-address-search > div.yolp-address-search-body")
                )
            )
        except TimeoutException as ex:
            ex.msg = \
                '[Time out][is_candidate_be_clicked]' + \
                "#map_canvas > div.yolp-address-search > div.yolp-address-search-body" + \
                '[winner: ' + str(self.winner) + ']'
            print(ex)
            return False
        return True

    def get_coors(self):
        try:
            lat = \
                WebDriverWait(self.driver, self.delay).until(
                    EC.presence_of_element_located(
                        (By.CSS_SELECTOR, "#indicate_lat")
                    )
                )

            long = \
                WebDriverWait(self.driver, self.delay).until(
                    EC.presence_of_element_located(
                        (By.CSS_SELECTOR, "#indicate_lng")
                    )
                )
        except TimeoutException as ex:
            ex.msg = '[Time out][get_coors] id="indicate_lat" id="indicate_lng"'
            print(ex)
            return
        self.latitude = lat.text
        self.longitude = long.text


class Maps:
    url = r'https://map.yahoo.co.jp/maps'

    @classmethod
    def get_address(cls, coordinate, web_driver):
        try:
            coordinate_re = r'\s*\d+.\d+\s*,\s*\d+.\d+\s*'
            if coordinate is None \
                    or coordinate == '' \
                    or not re.match(pattern=coordinate_re, string=coordinate):
                return ''

            delay = float(get_config('delay'))
            web_driver.get(cls.url)

            search_box = cls.get_search_box(web_driver, delay)
            search_box.send_keys(coordinate + Keys.RETURN)

            try:
                address = WebDriverWait(web_driver, delay).until(
                    EC.presence_of_element_located((
                        By.CSS_SELECTOR, '#mapinf > div > div.keepOn.cf > div > a'
                    ))
                )
                return address.text
            except TimeoutException:
                return ''
        except Exception as ex:
            raise ex

    @staticmethod
    def get_search_box(web_driver, delay):
        try:
            return WebDriverWait(web_driver, delay).until(
                EC.presence_of_element_located((
                    By.CSS_SELECTOR, '#yschsp'
                ))
            )
        except TimeoutException as ex:
            raise ex


# from web_browser import Browser
#
# browser = Browser()
# coors = Coors(browser.driver)
#
# coors.get('千葉県柏市船戸３丁目')
# print(coors.latitude, coors.longitude)
# coors.driver.quit()
