"""Web browser controller
180207 - SonTLT - create
"""

import re
from selenium import webdriver

from config import get_config


class Browser:
    def __init__(self):
        self.kind = get_config('browser')
        self.driver = create_driver(self.kind)


def create_driver(kind='Chrome'):
    """kinds:
    > Chrome
    > Firefox
    """
    if re.match(r'\s*chrome\s*', kind, re.IGNORECASE):
        driver = webdriver.Chrome()
    elif re.match(r'\s*firefox\s*', kind, re.IGNORECASE):
        driver = webdriver.Firefox()
    else:
        driver = webdriver.Chrome()
    return driver
