"""Google Geocoding API
180207 - SonTLT - create
"""

import requests
from config import quote

_url = 'https://maps.googleapis.com/maps/api/geocode/'
_api_key = r'AIzaSyAGodgs9fxmHmZAW8xl2G3TjPLyb-AqyRM'


def geocode(address, language=None, output_format='json'):
    """geocoding api request"""
    try:
        params = {
            'address': quote(address, '"'),
            'language': language,
            'key': _api_key
        }
        return requests.get(
            _url + output_format,
            params=params
        )
    except Exception as ex:
        raise ex


def get_coordinate(address):
    if address is None \
    or address == '':
        return ''

    response = geocode(address).json()
    status = response['status']
    if status == 'OK':
        results = response['results']
        if len(results) > 1:
            return 'MULTIPLE_RESULTS'
        elif len(results) == 1:
            location = results[0]['geometry']['location']
            lat, lng = map(
                lambda i : '{:.6f}'.format(i),
                [location['lat'], location['lng']]
            )
            return ', '.join([lat, lng])
        else:
            raise Exception(
                '[google.py][get_coordinate]Google, get your shit (geocoding) together!' +
                '[address: ' + address + ']'
            )
    elif status == 'ZERO_RESULTS':
        return ''
    else:
        return status
