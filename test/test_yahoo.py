import sys
sys.path.append('../src')

import pytest
# from pytest_mock import mocker
from yahoo import Coors

@pytest.mark.parametrize('type, addr, expected', [
    ('sigle', '宮城県東松島市野蒜ケ丘３丁目', 1),
    ('sigle', '秋田県北秋田市中屋敷字観音堂後ノ沢', 1),
    ('multiple', '埼玉県川越市豊田本１丁目', 32),
    ('multiple', '宮城県仙台市若林区荒井３丁目', 62),
    ('not found', '宮城県加美郡加美町字小瀬岩城下', 0),
    ('not found', '宮城県石巻市二子１丁目', 0)
])
def test_count_candidates(type, addr, expected):
    t_Coors = Coors()
    t_Coors.driver.quit()
    assert t_Coors.count_candidates(t_Coors.driver, addr) == expected

@pytest.mark.parametrize('type, addr, expected', [
    ('multiple', '埼玉県川越市豊田本１丁目', [
        '埼玉県川越市豊田本1丁目',
        '埼玉県川越市豊田本1丁目1',
        '埼玉県川越市豊田本1丁目2',
        '埼玉県川越市豊田本1丁目3',
        '埼玉県川越市豊田本1丁目4',
    ]),
])
def test_update_candidate(type, addr, expected):
    t_Coors = Coors()
    t_Coors.begin_search(t_Coors.driver, addr)
    t_Coors.update_candidates()
    t_Coors.driver.quit()
    assert t_Coors.candidates == expected

@pytest.mark.parametrize('type, addr, expected', [
    # ('sigle', '宮城県東松島市野蒜ケ丘３丁目', '宮城県東松島市野蒜ケ丘3丁目'),
    ('sigle', '秋田県北秋田市中屋敷字観音堂後ノ沢', '秋田県北秋田市中屋敷観音堂後ノ沢（字）'),
    ('multiple', '埼玉県川越市豊田本１丁目', '埼玉県川越市豊田本1丁目'),
    ('multiple', '宮城県加美郡加美町字大谷地', None),
    # ('multiple', '宮城県仙台市若林区荒井３丁目', None),
    # ('not found', '宮城県加美郡加美町字小瀬岩城下', None),
    ('not found', '宮城県石巻市二子１丁目', None)
])
def test_get_candidates_choose_winner(type, addr, expected):
    t_Coors = Coors()
    t_Coors.get_candidates_choose_winner(addr)
    if t_Coors.winner > -1:
        winner_addr = t_Coors.candidates[t_Coors.winner]
    else:
        winner_addr = None
    t_Coors.driver.quit()
    assert winner_addr == expected

@pytest.mark.parametrize('type, addr, expected', [
    # ('sigle', '宮城県東松島市野蒜ケ丘３丁目', '38.377534, 141.144371'),
    # ('sigle', '秋田県北秋田市中屋敷字観音堂後ノ沢', '40.186145, 140.389117'),
    ('multiple', '埼玉県川越市豊田本１丁目', '35.909335, 139.467020'),
    ('multiple', '宮城県加美郡加美町字大谷地', ''),
    # ('multiple', '宮城県仙台市若林区荒井３丁目', ''),
    # ('not found', '宮城県加美郡加美町字小瀬岩城下', ''),
    # ('not found', '宮城県石巻市二子１丁目', '')
])
def test_get(type, addr, expected):
    t_Coors = Coors()
    t_Coors.get(addr)
    lat_long = ', '.join(
        value 
        for value in [t_Coors.latitude, t_Coors.longitude]
        if value != ''
    )
    t_Coors.driver.quit()
    assert lat_long == expected
