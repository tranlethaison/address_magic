"""
addr_magic main
> address magic

History
> 171020 - SonTLT - Create
"""
import sys
import os

from openpyxl import load_workbook
from excel_reader import ExcelReader
import numpy as np
import numpy.ma as ma

from getter import Getter
from config import create_config
from config import get_config
from stopwatch import Timer
import yahoo
from progress_bar import printProgressBar


def filter_nparray(np_array, exclude=[], include=[]):
    """Filter numpy array"""
    include_everything = len(include) == 0
    if include_everything:
        include_mask = False
    else:
        include_mask = True
        for value in include:
            include_mask &= (np_array != value)

    exclude_mask = False
    for value in exclude:
        exclude_mask |= (np_array == value)

    return ma.masked_array(np_array, include_mask | exclude_mask)

def main(argv):
    '''
    main of Address Magic
    :argv : InputExcel OutputExcel
    '''
    if len(argv[1:]) == 2:
        inputFile = argv[1]
        outputFile = argv[2]
    else:
        print('Usage: InputExcel OutputExcel')
        sys.exit(1)

    try:
        create_config()

        if not os.path.isfile(inputFile): raise FileNotFoundError(inputFile)

        # Start total timer
        timer_total = Timer()
        timer_total.start()

        # terminal print
        print('#########################')
        print('##                     ##')
        print('## Address Magic v.3.0 ##')
        print('##                     ##')
        print('############### SonTLT ##')
        print(timer_total.now('\nProcess start at'))

        getter = Getter()
        if  not getter.test_browser():
            print('Your web browser is not compatible.')
            return
        
        # Get configs
        original_addresses_column = get_config('original_addresses_column')
        google_coordinates_column = get_config('google_coordinates_column')
        yahoo_addresses_column = get_config('yahoo_addresses_column')
        start_from_row = int(get_config('start_from_row'))
        end_at_row = int(get_config('end_at_row'))
        to_use_google_geocoding_api = get_config('to_use_google_geocoding_api')

        reader = ExcelReader()
        # Start timer for each file
        timer = Timer()
        timer.start()
        # Read addresses list from excel file
        original_addrs = reader.read_col(inputFile, 0, original_addresses_column, start_from_row, end_at_row)
        if to_use_google_geocoding_api == '1':
            # Get list of coordinates by using Geocoding API
            coors = getter.get_google_coordinates2(original_addrs)
        else:
            # Get list of coordinates from Google Maps
            coors = getter.get_google_coordinates(original_addrs)
        # Get list of address from Yahoo Maps
        addrs_ls = getter.get_yahoo_address(coors)

        # Get coordinates from Yahoo loko
        print('> Getting coordinates from Yahoo')
        print('>> site:http://user.numazu-ct.ac.jp/~tsato/webmap/sphere/coordinates/yahoo_olp/')
        coors = np.array(coors)
        include = ['multiple results', '']
        include_mask = True
        for value in include:
            include_mask &= (coors != value)
        filtered_original_addrs = ma.masked_array(original_addrs, include_mask)
        yahoo_coors_getter = yahoo.Coors()
        yahoo_coors = []
        for i, addr in enumerate(filtered_original_addrs):
            if addr is not ma.masked:
                yahoo_coors_getter.get(addr)
                coor = ', '.join(
                    value
                    for value in [yahoo_coors_getter.latitude, yahoo_coors_getter.longitude]
                    if value != ''
                )
                yahoo_coors.append(coor)
            else:
                yahoo_coors.append('')
            printProgressBar(i+1, len(filtered_original_addrs), prefix='', suffix='', length = 25, timer=timer)
        yahoo_coors_getter.driver.quit()

        # Write coordinates and addresses to output file
        ope_wb = load_workbook(inputFile)
        sheet = ope_wb.worksheets[0]
        row_no = start_from_row
        row_count = len(coors)
        while row_no < (row_count + start_from_row):
            sheet[google_coordinates_column + str(row_no)].value = coors[row_no - start_from_row]
            sheet[yahoo_addresses_column + str(row_no)].value = addrs_ls[row_no - start_from_row]
            sheet['I' + str(row_no)].value = yahoo_coors[row_no - start_from_row]
            row_no += 1
        print('> Saving to', outputFile)
        ope_wb.save(outputFile)
        print('> DONE!')
        print(timer.elapsed())

        # Print total timer
        print(timer_total.elapsed('\nTotal elapsed: '))
        print('Thank You for using! (^_^)/')
    except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            raise Exception(fname, exc_tb.tb_lineno, sys.exc_info())

if __name__ == '__main__':
    main(sys.argv)
