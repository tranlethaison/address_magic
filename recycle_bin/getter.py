"""
Getter class
> Geter magican
> Application's API key: AIzaSyAGodgs9fxmHmZAW8xl2G3TjPLyb-AqyRM

History
> 171020 - SonTLT - Create
> 171024 - SonTLT - Add Coordinates Getter
> 171101 - SonTLT - Add geocoding, get_coor2

"""
import os
import sys
import re
import requests
from time import sleep

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from config import get_config, quote, create_driver
from progress_bar import printProgressBar
from stopwatch import Timer
import google


class Getter:
    '''
    Getter class
    > address geter
    '''
    @staticmethod
    def get_yahoo_address(coors):
        '''
        using Yahoo Maps to get address from coordinates
        Yahoo Maps: http://map.yahoo.co.jp/maps?
        :param coors: list of coordinates (latitude, longitude)
        :return list of addresses
        '''
        try:
            timer = Timer()
            timer.start()

            print('> Getting addresses from Yahoo Maps')

            addresses = []
            delay = int(get_config('delay'))
            coordinates_re = r'\s*\d+.\d+\s*,\s*\d+.\d+\s*'

            # Choose browser driver
            browser = get_config('browser')
            driver = create_driver(browser)

            # loop: list of coordinates
            count = 1
            len_ = len(coors)
            for coor in coors:
                printProgressBar(count, len_, prefix='', suffix='', length = 25, timer=timer)
                count += 1
                # Find the search box
                # Paste in coordinates & press enter
                if not coor:
                    coor = ''

                if not re.match(
                        coordinates_re,
                        coor,
                        flags=0
                        ):
                    address = ''
                    addresses.append(address)
                    continue

                driver.get('http://map.yahoo.co.jp/maps?')

                search_box = driver.find_element_by_name('p')
                search_box.send_keys(coor + Keys.RETURN)

                # Find address
                # In case time out, return: blank
                try:
                    search_list = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'searchList')))
                    address = search_list.text.split('\n')
                    if len(address) >= 2:
                        address = address[1]
                    else:
                        address = ''
                except TimeoutException:
                    address = ''

                addresses.append(address)

            driver.quit()

            return addresses
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            raise Exception(fname, exc_tb.tb_lineno, sys.exc_info())


    @staticmethod
    def get_google_coordinates(addresses):
        """
        > Get coordinates from address, using Google Maps (http://www.google.co.jp/maps)
        :param addresses: list of addresses
        :return list of coordinates (latitude, longitude)
        """
        try:
            timer = Timer()
            timer.start()

            print('> Getting coordinates from Google Maps')

            # Choose browser driver
            browser = get_config('browser')
            driver = create_driver(browser)

            # list of coordinates
            coors = []

            # Google Maps is like 2 times slower than Yahoo Maps
            delay = 2 * int(get_config('delay'))

            coors_found_url_re = r'.+/place/.+!\d+[a-zA-Z]+(\d+.\d+)!\d+[a-zA-Z]+(\d+.\d+).*'
            coors_not_found_url_re = r'.+/search/.+@\d+.\d+,\d+.\d+.*'

            # use only to printProgressBar
            count = 1
            len_ = len(addresses)

            # loop: list of addresses
            first = True
            for address in addresses:
                printProgressBar(count, len_, prefix='', suffix='', length = 25, timer=timer)
                count += 1

                # Skip if address is blank
                if not address:
                    address = ''

                if address == '':
                    coor = ''
                    coors.append(coor)
                    continue

                driver.get('http://www.google.co.jp/maps')
                # First time loading is usually slower
                if first:
                    sleep(delay)
                    first = False

                # Find the search box
                # Paste in address & press Enter
                try:
                    search_box = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'searchboxinput')))
                    search_box.send_keys(address + Keys.RETURN)
                except TimeoutException:
                    coors.append('time out')
                    continue

                # Try to determine if coordinates are found by URL. By a delay time.
                time_out = True
                try_count = 0
                while try_count < delay:
                    try_count +=1
                    sleep(1)
                    url = driver.current_url

                    # print(try_count, url)

                    # Coordinates are found
                    if re.match(coors_found_url_re, url, flags=0):
                        # the list of (latitude, longitude)
                        coor_list = re.search(coors_found_url_re, url, flags=0)

                        if coor_list:
                            coor = coor_list[1] + ', ' + coor_list[2]
                        else:
                            coor = ''

                        coors.append(coor)
                        time_out = False
                        break
                    # Not found
                    elif re.match(coors_not_found_url_re, url, flags=0):
                        coors.append('')
                        time_out = False
                        break

                if time_out:
                    coors.append('time out')

            driver.quit()

            return coors
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            raise Exception(fname, exc_tb.tb_lineno, sys.exc_info())

    @staticmethod
    def get_google_coordinates2(addresses):
        """
        > Get coordinates from address, using Google Geocoding API
        :param addresses: list of addresses
        :return list of coordinates (latitude, longitude)
        """
        try:
            timer = Timer()
            timer.start()

            print('> Getting coordinates by using Google Geocoding API')

            geocoder = google.Google()

            # list of coordinates
            coors = []

            # use only to printProgressBar
            count = 1
            len_ = len(addresses)

            # loop: list of addresses
            for address in addresses:
                printProgressBar(count, len_, prefix='', suffix='', length = 25, timer=timer)
                count += 1

                # Skip if address is blank
                if not address:
                    address = ''

                if address == '':
                    coors.append('')
                    continue

                # geocoding request
                # quote address be4 geocoding
                address = quote(address, '"')
                responses = geocoder.geocode(address=address).json()

                status = responses['status']

                if status == 'OK':
                    results = responses['results']
                    results_len = len(results)

                    # Get coordinates if there's only 1 result.
                    # Otherwise, treat as not found.
                    if results_len == 1:
                        location = results[0]['geometry']['location']
                        coor = ', '.join([
                            "{:.6f}".format(value) 
                            for value in [location['lat'], location['lng']]
                        ])
                        coors.append(coor)
                    else:
                        coors.append('multiple results')
                        continue
                elif status == 'ZERO_RESULTS':
                    coors.append('')
                    continue
                else:
                    coors.append(status)
                    continue

            return coors
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            raise Exception(fname, exc_tb.tb_lineno, sys.exc_info())

    @staticmethod
    def test_browser():
        '''
        Test if user's broswer is compatible with Selenium driver
        '''
        try:
            # Choose browser driver
            browser = get_config('browser')
            driver = create_driver(browser)

            driver.get('https://www.google.com')
            driver.quit()

            return True
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(fname, exc_tb.tb_lineno, sys.exc_info())
            return False
