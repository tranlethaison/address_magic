"""
ExcelReader class
> Excel reader

History
> 171019 - SonTLT - Create
> 171024 - SonTLT - Modify to read excel file on common, not just read coordinates
"""
import sys
import os

from openpyxl import load_workbook

from progress_bar import printProgressBar
from stopwatch import Timer


class ExcelReader:
    """
    ExcelReader class
    > Excel reader
    """

    def read_col(self, excel_f, sheet_no, col, start_from_row, end_at_row):
        """
        read a column from excel files
        :param excel_f: excel file path
        :param sheet_no: no. of work sheet
        :param col: the column
        :param start_from_row: starting row
        :param end_at_row: endind row
        :return list of values
        """
        try:
            timer = Timer()
            timer.start()
            print('\n' + excel_f)

            # list values of column
            values = []

            workbook = load_workbook(excel_f)
            sheet = workbook.worksheets[sheet_no]

            max_row = len(sheet[col])

            if end_at_row > max_row:
                end_at_row = max_row
            
            print('> Reading column', col + '. From cell', start_from_row, 'to cell', end_at_row)

            # loop through cells of col
            # get the value and add to list
            cel_cnt = start_from_row
            while cel_cnt <= end_at_row:
                value = sheet[col + str(cel_cnt)].value
                values.append(value)

                printProgressBar(cel_cnt - start_from_row + 1, end_at_row - start_from_row + 1, prefix='', suffix='', length = 25, timer=timer)
                cel_cnt += 1

            return values
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            raise Exception(fname, exc_tb.tb_lineno, sys.exc_info())
