'''
> Progress bar

> History:
>> 171101   SonTLT  Add item no., timer
'''
# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', timer=None):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar_ = fill * filledLength + '-' * (length - filledLength)
    # print('\r%s |%s| %s%% %s' % (prefix, bar_, percent, suffix), end = '\r')
    if timer != None:
        print('\r%s |%s| %s%% %s' % (prefix, bar_, percent, suffix), str(iteration) + '/' + str(total), timer.elapsed(), end = '\r')
    else:
        print('\r%s |%s| %s%% %s' % (prefix, bar_, percent, suffix), str(iteration) + '/' + str(total), end = '\r')

    # Print New Line on Complete
    if iteration == total:
        print()
